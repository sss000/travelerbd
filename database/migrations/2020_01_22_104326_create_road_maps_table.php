<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoadMapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('road_maps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('historical_id')->nullable();
            $table->unsignedBigInteger('hotel_id')->nullable();
            $table->unsignedBigInteger('transport_id')->nullable();
            $table->string('name',100);
            $table->float('distance',8,2);
            $table->integer('cost');
            $table->text('map')->nullable();
            $table->foreign('historical_id')->references('id')->on('historical_places');
            $table->foreign('hotel_id')->references('id')->on('hotels');
            $table->foreign('transport_id')->references('id')->on('transports');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('road_maps');
    }
}
