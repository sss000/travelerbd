<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'TravelerBd') }}</title>

      <link rel="stylesheet" href="{{ asset('css/app.css') }}">
      
</head>
<body>
    <div class="wrapper" id="app">
        

        <main>
            @yield('content')
        </main>
        
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
