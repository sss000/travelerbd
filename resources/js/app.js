
require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router'
import Vue2Editor from "vue2-editor";

Vue.use(Vue2Editor);
Vue.use(VueRouter)
import {routes} from './routes';

//vuex
import storeData from "./index"
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store(
	storeData
)

//moment js
import moment from 'moment'
Vue.filter('shortLength',(text,lenght,suffix)=>{
	return text.substring(0,lenght)+suffix;
})

//v-form
import { Form, HasError, AlertError } from 'vform'
window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

//sweetalart2
import Swal from 'sweetalert2'
window.Swal=Swal;
const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})



window.Toast= Toast;
//Vue.component('example-component', require('./components/ExampleComponent.vue').default);
//Vue.component('test', require('./components/backend/test.vue').default);


const router = new VueRouter({

	routes,
	mode:'hash'
});

const app = new Vue({ 
    router,
    store

}).$mount('#app')
