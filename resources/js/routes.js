//all component
import list from './components/backend/spot/list.vue'
import hotel_list from './components/backend/hotel/list.vue'
import transport_list from './components/backend/transport/transport.vue'
import road_list from './components/backend/roadMap/list.vue'

export const routes = [
	//all routes

	{

		path:'/list',
		component:list
	},
	
	
	{
		path:'/hotel_list',
		component:hotel_list
	},

	{
		path:'/transport_list',
		component:transport_list
	},
	
	{
		path:'/road_list',
		component:road_list
	}

	
]