export default{

	state:{
		allSpots:[],
		allHotels:[],
		allTransports:[],
		allRoads:[]
	},

	getters:{

		getSpot(state){
			return state.allSpots
		},
		
		getHotel(state){
			return state.allHotels
		},
		
		getTransport(state){
			return state.allTransports
		},

		getRoad(state){
			return state.allRoads
		}
	},

	actions:{
		spots(context){

			axios.get('/list')
			.then((response)=>{
				context.commit('datas',response.data.datas)
			})
		},
		hotels(context){

			axios.get('/hotel_list')
			.then((response)=>{
				context.commit('hotel_datas',response.data.hotel_datas)
			})
		},
		transports(context){

			axios.get('/transport_list')
			.then((response)=>{
				context.commit('transport_datas',response.data.transport_datas)
			})
		},
		roads(context){

			axios.get('/road_list')
			.then((response)=>{
				context.commit('road_datas',response.data.road_datas)
			})
		}

	},

	mutations:{

		datas(state,data){
            return state.allSpots = data
        },
		
		hotel_datas(state,data){
            return state.allHotels = data
        },
        
        transport_datas(state,data){
            return state.allTransports = data
        },

        road_datas(state,data){
            return state.allRoads = data
        }

	}
}