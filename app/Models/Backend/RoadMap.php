<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class RoadMap extends Model
{
    use Notifiable;

    protected $fillable = [
        'historical_id','hotel_id','transport_id','name','distance','cost','map'
    ];


    public function historical(){

    	return $this->belongsTo(HistoricalPlace::class);
    }

    public function hotel(){

    	return $this->belongsTo(Hotel::class);
    }

    public function transport(){

    	return $this->belongsTo(Transport::class);
    }
}
