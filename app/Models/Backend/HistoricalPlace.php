<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class HistoricalPlace extends Model
{
    use Notifiable;

    protected $fillable = [
        'name', 'address', 'description','map','cover_photo'
    ];


    public function hotels(){

    	return $this->hasMany(Hotel::class,'id');
    }

    public function transports(){

    	return $this->hasMany(Transport::class,'id');
    }

    public function road(){

        return $this->hasMany(RoadMap::class,'id');
    }
}
