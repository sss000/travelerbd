<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Transport extends Model
{
    use Notifiable;

    protected $fillable = [
        'name', 'address','distance','cost', 'description','map','cover_photo','historical_id'
    ];

    public function historical(){

    	return $this->belongsTo(HistoricalPlace::class);
    }

    public function road(){

        return $this->hasMany(RoadMap::class,'id');
    }
}
