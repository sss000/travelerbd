<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Backend\RoadMap;
use Illuminate\Support\Str;
class RoadMapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function road_index()
    {
        $road_datas = RoadMap::with('historical','transport','hotel')->orderBy('id','desc')->get();
        return response()->json([
           'road_datas'=>$road_datas
       ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function road_store(Request $request)
    {
        $this->validate($request,[
            'historical_id'=>'required',
            'name'=>'required|min:2|max:100',
            'distance'=>'required',
            'cost'=>'required'        
        ]);
        $post = new RoadMap();
        $post->historical_id = $request->historical_id;
        $post->transport_id = $request->transport_id;
        $post->hotel_id= $request->hotel_id;
        $post->name = $request->name;
        $post->distance = $request->distance;
        $post->cost = $request->cost;
        $post->map = $request->map;
        $post->save();
        
        return response(['status'=>'Road Uploaded Successfully.'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function road_edit($id)
    {
        $road_data = RoadMap::find($id);
        return response()->json([
           'road_data'=>$road_data
       ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function road_update(Request $request, $id)
    {
        $post = RoadMap::find($id);

        $this->validate($request,[
            'historical_id'=>'required',
            'name'=>'required|min:2|max:100',
            'distance'=>'required',
            'cost'=>'required'        
        ]);
        
        $post->historical_id = $request->historical_id;
        $post->transport_id = $request->transport_id;
        $post->hotel_id= $request->hotel_id;
        $post->name = $request->name;
        $post->distance = $request->distance;
        $post->cost = $request->cost;
        $post->map = $request->map;
        $post->save();
        
        return response(['status'=>'Road Uploaded Successfully.'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function road_destroy($id)
    {
        $data = RoadMap::find($id);
        
        $data->delete();
    }
}
