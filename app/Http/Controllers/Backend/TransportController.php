<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Backend\Transport;
use Image;
use Illuminate\Support\Str;
class TransportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function transport_index()
    {
        $transport_datas = Transport::with('historical')->orderBy('id','desc')->get();
        return response()->json([
           'transport_datas'=>$transport_datas
       ],200);
    }

    
    public function create()
    {
        //
    }

    public function transport_store(Request $request)
    {
        $this->validate($request,[
            'historical_id'=>'required',
            'name'=>'required|min:2|max:100',
            'address'=>'required|min:2|max:100',
            'description'=>'required|min:2'        
        ]);

        if($request->get('cover_photo'))
       {
          $image1 = $request->get('cover_photo');
          $name1 = Str::random(5).'.' . explode('/', explode(':', substr($image1, 0, strpos($image1, ';')))[1])[1];
          $img1 = Image::make($request->cover_photo)->resize(300, 300);
          $upload_path1 = public_path()."/images/";
          $img1->save($upload_path1.$name1);

          // $image2 = $request->get('image2');
          // $name2 = Str::random(5).'.' . explode('/', explode(':', substr($image2, 0, strpos($image2, ';')))[1])[1];

          // $img2 = Image::make($request->image2)->resize(300, 300);
          // $upload_path2 = public_path()."/images/";
          // $img2->save($upload_path2.$name2);

          // $image3 = $request->get('image3');
          // $name3 = Str::random(5).'.' . explode('/', explode(':', substr($image3, 0, strpos($image3, ';')))[1])[1];
          // $img3 = Image::make($request->image3)->resize(300, 300);
          // $upload_path3 = public_path()."/images/";
          // $img3->save($upload_path3.$name3);
  
        }
        
        $post = new Transport();
        $post->historical_id = $request->historical_id;
        $post->name = $request->name;
        $post->address = $request->address;
        $post->description = $request->description;
        $post->map = $request->map;
        $post->cover_photo = $name1;
        $post->save();
        
        return response(['status'=>'Spot Uploaded Successfully.'],200);
    }

    
    public function show($id)
    {
        //
    }

    
    public function transport_edit($id)
    {
        $transport_data = Transport::find($id);
        return response()->json([
           'transport_data'=>$transport_data
       ],200);
    }

   
    public function transport_update(Request $request, $id)
    {
        $post = Transport::find($id);

        $this->validate($request,[
            'historical_id'=>'',
            'name'=>'required|min:2|max:100',
            'address'=>'required|min:2|max:100',
            'description'=>'required|min:2'  
        ]);

      if ($request->cover_photo != $post->cover_photo) {

          $image = $request->get('cover_photo');
          $name1 = Str::random(5).'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];

          $img = Image::make($request->cover_photo)->resize(300, 300);
          $upload_path = public_path('images/');
          $img->save($upload_path.$name1);
          $path = public_path('images/').$post->cover_photo;

          if (file_exists($path)) {
            @unlink($path);
          }
      }else{
        $name1 = $post->cover_photo;
      }

      $post->historical_id = $request->historical_id;
      $post->name = $request->name;
      $post->address = $request->address;
      $post->description = $request->description;
      $post->map = $request->map;
      $post->cover_photo = $name1;
      $post->save();
      
      return response(['status'=>'Spot Uploaded Successfully.'],200);
    }

    
    public function transport_destroy($id)
    {
        $data = Transport::find($id);
        $image_path1 = public_path('images/').$data->cover_photo;

        if (file_exists($image_path1)) {
            @unlink($image_path1);
        }
        

        $data->delete();
    }
}
