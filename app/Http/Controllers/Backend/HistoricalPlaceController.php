<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Backend\HistoricalPlace;
use Image;
use Illuminate\Support\Str;
class HistoricalPlaceController extends Controller
{
    
    //abc 


    public function all_spots()
    {
        $datas = HistoricalPlace::orderBy('id','desc')->get();
        return response()->json([
           'datas'=>$datas
       ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        
    }

    
    public function spot_store(Request $request)
    {

        $this->validate($request,[
            'name'=>'required|min:2|max:100',
            'address'=>'required|min:2|max:100',
            'description'=>'required|min:2'        
        ]);


       if($request->get('cover_photo'))
       {
          $image1 = $request->get('cover_photo');
          $name1 = Str::random(5).'.' . explode('/', explode(':', substr($image1, 0, strpos($image1, ';')))[1])[1];
          $img1 = Image::make($request->cover_photo)->resize(300, 300);
          $upload_path1 = public_path()."/images/";
          $img1->save($upload_path1.$name1);

          // $image2 = $request->get('image2');
          // $name2 = Str::random(5).'.' . explode('/', explode(':', substr($image2, 0, strpos($image2, ';')))[1])[1];
          // $img2 = Image::make($request->image2)->resize(300, 300);
          // $upload_path2 = public_path()."/images/";
          // $img2->save($upload_path2.$name2);

          // $image3 = $request->get('image3');
          // $name3 = Str::random(5).'.' . explode('/', explode(':', substr($image3, 0, strpos($image3, ';')))[1])[1];
          // $img3 = Image::make($request->image3)->resize(300, 300);
          // $upload_path3 = public_path()."/images/";
          // $img3->save($upload_path3.$name3);
        }
        
        $post = new HistoricalPlace();
        $post->name = $request->name;
        $post->address = $request->address;
        $post->description = $request->description;
        $post->map = $request->map;
        $post->cover_photo = $name1;
        $post->save();
        
        return response(['status'=>'Spot Uploaded Successfully.'],200);
        
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit_spot($id)
    {
        $spot_data = HistoricalPlace::find($id);
        return response()->json([
           'spot_data'=>$spot_data
       ],200);
    }

    
    public function update_spot(Request $request, $id)
    {
      $post = HistoricalPlace::find($id);

        $this->validate($request,[
            'name'=>'required|min:2|max:100',
            'address'=>'required|min:2|max:100',
            'description'=>'required|min:2'

            
        ]);

      if ($request->cover_photo != $post->cover_photo) {

          $image = $request->get('cover_photo');
          $name1 = Str::random(5).'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          //Image::make($request->get('image'))->save(public_path('images/'.$name));

          $img = Image::make($request->cover_photo)->resize(300, 300);
          $upload_path = public_path('images/');
          $img->save($upload_path.$name1);
          $path = public_path('images/').$post->cover_photo;

          if (file_exists($path)) {
            @unlink($path);
          }
      }else{
         $name1 = $post->cover_photo;
      }
        

      // if ($request->image2!=$post->image2) {
       
      //     $image = $request->get('image2');
      //     $name2 = Str::random(5).'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];

      //     $img = Image::make($request->image2)->resize(300, 300);
      //     $upload_path = public_path('images/');
      //     $img->save($upload_path.$name2);
      //     $path = public_path('images/').$post->image2;

      //     if (file_exists($path)) {
      //       @unlink($path);
      //     }
      // }else{
      //    $name2 = $post->image2;
      // }

      // if ($request->image3!=$post->image3) {
      
      //     $image = $request->get('image3');
      //     $name3 = Str::random(5).'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];

      //     $img = Image::make($request->image3)->resize(300, 300);
      //     $upload_path = public_path('images/');
      //     $img->save($upload_path.$name3);
      //     $path = public_path('images/').$post->image3;

      //     if (file_exists($path)) {
      //       @unlink($path);
      //     }
          
      // }else{
      //    $name3 = $post->image3;
      // }
      
      $post->name = $request->name;
      $post->address = $request->address;
      $post->description = $request->description;
      $post->map = $request->map;
      $post->cover_photo = $name1;
      $post->save();
     
      return response(['status'=>'Spot Uploaded Successfully.'],200);
    }

    
    public function delete_spot($id)
    {
        $data = HistoricalPlace::find($id);
        $image_path1 = public_path('images/').$data->cover_photo;

        if (file_exists($image_path1)) {
            @unlink($image_path1);
        }

        $data->delete();
    }
}
