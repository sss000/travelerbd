<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//spot route.....
Route::get('/list','Backend\HistoricalPlaceController@all_spots')->name('all_spots');
Route::post('/spot_store','Backend\HistoricalPlaceController@spot_store')->name('spot_store');
Route::get('/spot_data/{id}','Backend\HistoricalPlaceController@edit_spot')->name('edit_spot');
Route::put('/update_spot/{id}','Backend\HistoricalPlaceController@update_spot')->name('update_spot');
Route::get('/delete_spot/{id}','Backend\HistoricalPlaceController@delete_spot')->name('delete_spot');

//hotel route....
Route::get('/hotel_list','Backend\HotelController@hotel_list')->name('hotel_list');
Route::post('/hotel_store','Backend\HotelController@hotel_store')->name('hotel_store');
Route::get('/hotel/{id}','Backend\HotelController@edit_hotel')->name('edit_hotel');
Route::put('/update/{id}','Backend\HotelController@hotel_update')->name('hotel_update');
Route::get('/hotel_destroy/{id}','Backend\HotelController@hotel_destroy')->name('hotel_destroy');

//transport route...
Route::get('/transport_list','Backend\TransportController@transport_index')->name('transport_index');
Route::post('/transport_store','Backend\TransportController@transport_store')->name('transport_store');
Route::get('/transport_edit/{id}','Backend\TransportController@transport_edit')->name('transport_edit');
Route::put('/transport_update/{id}','Backend\TransportController@transport_update')->name('transport_update');
Route::get('/transport_destroy/{id}','Backend\TransportController@transport_destroy')->name('transport_destroy');

//transport route...
Route::get('/road_list','Backend\RoadMapController@road_index')->name('road_index');
Route::post('/road_store','Backend\RoadMapController@road_store')->name('road_store');
Route::get('/road_edit/{id}','Backend\RoadMapController@road_edit')->name('road_edit');
Route::put('/road_update/{id}','Backend\RoadMapController@road_update')->name('road_update');
Route::get('/road_destroy/{id}','Backend\RoadMapController@road_destroy')->name('road_destroy');